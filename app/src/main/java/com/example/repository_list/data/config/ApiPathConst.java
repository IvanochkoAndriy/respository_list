package com.example.repository_list.data.config;

interface ApiPathConst {
    String API_BASE_URL = "";
    String API_PATH = "";
    String HEADER_SESSION_TOKEN = "Authorization";
    String HEADER_TOKEN_PREFIX = "Token token=";
    int CONNECTION_TIMEOUT = 25; //sec
}
