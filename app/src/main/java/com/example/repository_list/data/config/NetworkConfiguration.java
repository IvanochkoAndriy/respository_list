package com.example.repository_list.data.config;

import java.util.List;

import okhttp3.Interceptor;

public interface NetworkConfiguration {
    int getConnectionTimeout();
    String getBaseUrl();
    List<Interceptor> getInterceptors();
}
