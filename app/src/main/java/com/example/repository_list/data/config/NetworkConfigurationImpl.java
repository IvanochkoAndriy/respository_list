package com.example.repository_list.data.config;

import com.example.repository_list.BuildConfig;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.Request;

public class NetworkConfigurationImpl implements NetworkConfiguration{
    @Override
    public int getConnectionTimeout() {
        return ApiPathConst.CONNECTION_TIMEOUT;
    }
    private String getAuthTokenHeaderKey() {
        return ApiPathConst.HEADER_SESSION_TOKEN;
    }
    private String getAuthToken() {
        return ApiPathConst.HEADER_TOKEN_PREFIX;
    }
    @Override
    public String getBaseUrl() {
        return ApiPathConst.API_BASE_URL + ApiPathConst.API_PATH;
    }
    @Override
    public List<Interceptor> getInterceptors() {
        List<Interceptor> interceptors = new ArrayList<>();
        interceptors.add(getAuthRequestInterceptor());
        return interceptors;
    }

    private Interceptor getAuthRequestInterceptor() {
        return chain -> {
            Request request = chain.request();
            final String authHeaderKey = getAuthTokenHeaderKey();
            final String authHeaderToken = getAuthToken();
            if (authHeaderKey != null && authHeaderToken != null) {
                request = request.newBuilder()
                        .addHeader(authHeaderKey, authHeaderToken)
                        .build();
            }
            return chain.proceed(request);
        };
    }}
