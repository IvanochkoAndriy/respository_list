package com.example.repository_list.di.components;

import com.example.repository_list.di.modules.ApplicationModule;
import com.example.repository_list.di.modules.NetworkModule;
import com.example.repository_list.di.modules.RepositoryModule;
import com.example.repository_list.presentation.App;

import dagger.Component;

@Component(modules = {ApplicationModule.class, NetworkModule.class, RepositoryModule.class})
public
interface ApplicationComponent {
    void inject(App app);

    ActivityComponent plusActivityComponent();
}
