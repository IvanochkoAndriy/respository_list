package com.example.repository_list.di.modules;

import com.example.repository_list.data.config.NetworkConfigurationImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Singleton
    @Provides
    OkHttpClient provideOkhttpClient(NetworkConfigurationImpl configuration){
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .writeTimeout(configuration.getConnectionTimeout(), TimeUnit.SECONDS)
                .readTimeout(configuration.getConnectionTimeout(), TimeUnit.SECONDS);

        for(Interceptor interceptor : configuration.getInterceptors()){
            builder.addInterceptor(interceptor);
        }

        return builder.build();
    }

    @Singleton
    @Provides
    Retrofit provideRetrofit(OkHttpClient client, NetworkConfigurationImpl configuration){
        Gson gson = new GsonBuilder().create();

        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(configuration.getBaseUrl())
                .client(client)
                .build();
    }
}
