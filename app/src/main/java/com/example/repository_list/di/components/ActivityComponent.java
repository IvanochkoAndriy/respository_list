package com.example.repository_list.di.components;

import com.example.repository_list.di.modules.ActivityModule;

import dagger.Subcomponent;

@Subcomponent(modules = {ActivityModule.class})
public interface ActivityComponent {
}
