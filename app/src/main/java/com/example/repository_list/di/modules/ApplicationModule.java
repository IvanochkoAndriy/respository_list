package com.example.repository_list.di.modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private Application mApplication;

    public ApplicationModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Singleton
    @Provides
    Context provideContext(){
        return mApplication;
    }

    @Singleton
    @Provides
    Application provideApplication(){
        return mApplication;
    }
}
