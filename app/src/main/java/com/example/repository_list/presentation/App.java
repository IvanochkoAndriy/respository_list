package com.example.repository_list.presentation;

import android.app.Application;

import com.example.repository_list.di.components.ApplicationComponent;
import com.example.repository_list.di.components.DaggerApplicationComponent;
import com.example.repository_list.di.modules.ApplicationModule;

public class App extends Application {
    private  static App mInstance;
    protected ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        getApplicationComponent().inject(this);
    }

    public static App getInstance() {
        return mInstance;
    }

    public ApplicationComponent getApplicationComponent(){
        if(mApplicationComponent == null){
            mApplicationComponent = DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(this))
                    .build();
        }

        return mApplicationComponent;
    }
}
